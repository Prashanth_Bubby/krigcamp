from django.apps import AppConfig


class KrigcampConfig(AppConfig):
    name = 'KRIGCAMP'
