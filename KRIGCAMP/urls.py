from django.conf.urls import url
from django.contrib.auth.views import login
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/',login ,{'template_name':'signin.html/'}),
    url(r'^signup_usr/', views.signup_usr, name='signup'),
    url(r'^blog/', views.blog, name='blog'),
    url(r'^signup/', views.signup, name='sign'),
]
